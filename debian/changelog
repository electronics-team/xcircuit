xcircuit (3.9.73+dfsg.1-1) unstable; urgency=medium

  * New upstream release (Closes: #909520)
  * Salvage package by adopting into the Debian Electronics team
    (Closes: #909678)
  * debian/changelog: Remove some unneeded whitespaces reported by Lintian.
  * debian/control:
    - Change Maintainer / Uploaders
    - debhelper >= 11
    - Change formatting
    - New standards version 4.2.1 - no changes
    - Vcs URLs added
    - Priority is optional
  * debian/copyright:
    - Set Format field to the officially valid field
    - Change formatting to please lintian
    - Added copyright information for a few more files
    - List a few excluded files to be excluded when repackaging DFSG compliant
      tarball
  * debian/gbp.conf: To enforce pristine-tar
  * debian/patches:
    - Refreshed patches
    - Spelling fixes
  * debian/rules:
    - Enable some hardening
    - Drop get-orig-source target
    - Remove unneeded commands in override_dh_auto_configure
    - Set "--with autoreconf"
  * debian/watch:
    - Update to point to stable release series (Closes: #909515)

 -- Ruben Undheim <rubund@debian.org>  Thu, 01 Nov 2018 23:32:47 +0100

xcircuit (3.8.78.dfsg-1) unstable; urgency=medium

  * New upstream release
  * debian/control:
    - Standards-Version: 3.9.6
    - Depends: wish (Closes: #770587)

 -- Roland Stigge <stigge@antcom.de>  Thu, 14 Jan 2016 22:14:05 +0100

xcircuit (3.7.57.dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Sun, 18 May 2014 19:56:04 +0200

xcircuit (3.7.56.dfsg-1) unstable; urgency=medium

  * New upstream release
    - API adjustment (Closes: #741821)
    - Dependencies fixed (Closes: #742495)
  * Applied Tcl 8.6 fix by Sergei Golovan (Closes: #724826)
  * Use chrpath to strip RPATH from /usr/lib/xcircuit/xcircexec

 -- Roland Stigge <stigge@antcom.de>  Mon, 07 Apr 2014 11:46:15 +0200

xcircuit (3.7.55.dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version: 3.9.5

 -- Roland Stigge <stigge@antcom.de>  Sat, 14 Dec 2013 12:24:06 +0100

xcircuit (3.7.54.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Mon, 21 Oct 2013 11:20:36 +0200

xcircuit (3.7.52.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Thu, 17 Oct 2013 15:49:35 +0200

xcircuit (3.7.51.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Sun, 28 Jul 2013 16:12:10 +0200

xcircuit (3.7.50.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Fri, 14 Jun 2013 21:08:34 +0200

xcircuit (3.7.49.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Sat, 18 May 2013 16:24:01 +0200

xcircuit (3.7.48.dfsg-3) unstable; urgency=low

  * Added patch to fix foreground / background readability issue, thanks to
    Tim Edwards (Closes: #)

 -- Roland Stigge <stigge@antcom.de>  Thu, 16 May 2013 20:48:16 +0200

xcircuit (3.7.48.dfsg-2) unstable; urgency=low

  * debian/control: Standards-Version: 3.9.4

 -- Roland Stigge <stigge@antcom.de>  Wed, 15 May 2013 21:06:18 +0200

xcircuit (3.7.48.dfsg-1) experimental; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Fri, 25 Jan 2013 11:17:32 +0100

xcircuit (3.7.47.dfsg-1) experimental; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Sun, 06 Jan 2013 14:47:55 +0100

xcircuit (3.7.46.dfsg-1) experimental; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Mon, 31 Dec 2012 13:44:36 +0100

xcircuit (3.7.45.dfsg-1) experimental; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Sun, 23 Dec 2012 19:26:52 +0100

xcircuit (3.7.44.dfsg-1) experimental; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Wed, 03 Oct 2012 12:16:57 +0200

xcircuit (3.7.43.dfsg-1) experimental; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Tue, 25 Sep 2012 13:50:18 +0200

xcircuit (3.7.41.dfsg-1) experimental; urgency=low

  * New upstream release
  * debian/control, debian/rules: Force tk8.5 and tcl8.5 since 8.6 leads to X
    errors (BadValue)

 -- Roland Stigge <stigge@antcom.de>  Fri, 17 Aug 2012 10:38:16 +0200

xcircuit (3.7.40.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Mon, 28 May 2012 12:03:03 +0200

xcircuit (3.7.39.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Fri, 06 Apr 2012 14:39:32 +0200

xcircuit (3.7.37.dfsg-1) unstable; urgency=low

  * New upstream release
  * Dropped 02_fix_tcl_libs.patch (integrated upstream)

 -- Roland Stigge <stigge@antcom.de>  Sun, 25 Mar 2012 12:47:54 +0200

xcircuit (3.7.35.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Fri, 23 Mar 2012 11:18:44 +0100

xcircuit (3.7.33.dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/compat: 9
  * Added patch to fix string format issues (05_fix_string_format.patch)

 -- Roland Stigge <stigge@antcom.de>  Sat, 10 Mar 2012 21:17:32 +0100

xcircuit (3.7.32.dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version: 3.9.3
  * debian/rules: Added get-orig-source target

 -- Roland Stigge <stigge@antcom.de>  Sat, 25 Feb 2012 21:54:52 +0100

xcircuit (3.7.30.dfsg-1) unstable; urgency=low

  * New upstream release
  * Fixed manpage (04_fix_manpage.patch)

 -- Roland Stigge <stigge@antcom.de>  Mon, 13 Feb 2012 21:04:00 +0100

xcircuit (3.7.26.dfsg-1) unstable; urgency=low

  * New upstream release (Closes: #636948)
  * debian/control: Uploaders: Roland Stigge <stigge@antcom.de>
  * debian/source/format: 3.0 (quilt)
  * Added tutorial.tar.gz (Closes: #72945)

 -- Roland Stigge <stigge@antcom.de>  Thu, 11 Aug 2011 22:27:10 +0200

xcircuit (3.6.135.dfsg-1) unstable; urgency=high

  * New upstream release.
    - Dropped debian/patches/fix_tk_version.dpatch, since it's been applied
      in the new upstream release.
  * Modified debian/rules to fix FTBFS in when sudo is used instead of
    fakeroot. (Closes: #481460)
  * Updated Standards Version to 3.8.0.
    - Removed "Conflicts: xbase (<< 3.3.2.3a-2)" from debian/control
  * Fixed some lintian warnings.
    + Added watch file.
    + Removed deprecated build-deps (x-dev, xutils), added x11proto-core-dev
      instead.
    + Fixed debian/rules so that config.sub and config.guess differences
      aren't shipped.
  * Made debian/copyright a machine parsable file.

 -- Margarita Manterola <marga@debian.org>  Sat, 28 Jun 2008 22:32:54 +0000

xcircuit (3.6.130.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Updated Standards Version.
    + Updated menu entries.
  * Added Homepage field
  * Several changes to debian/rules
    + Moved DH_COMPAT to debian/compat
    + Fixed the clean target so that the package can be built twice in row
      (Closes: #442767).
    + Fixed the install target so that the appdefaults are correctly
      installed, and menus work as expected (Closes: #102640).
    + Fix the configure target so that it finds tcl correctly. This fixes
      some of the reported segfaults (Closes: #418631, #441893).
  * Added debian/patches/fix_lib_files.dpatch, to be able to
    correctly install the files in /usr/share and /usr/lib.
  * Added debian/patches/fix_tk_version.dpatch, to allow the use of
    tcl/tk 8.5.2, which didn't work.

 -- Margarita Manterola <marga@debian.org>  Sun, 27 Apr 2008 02:53:28 +0000

xcircuit (3.6.78.dfsg-1) unstable; urgency=low

  * New upstream release.
  	- Works correctly in amd64 (Closes: #229955).
    - Drop python in favour of tcl support (Closes: #380989, #226233).
  * New maintainer (Closes: #401609).
  * Migrated from cdbs to debhelper.
  * Repackaged original sources due to licensing problems of the asg module.
  * Added extra menu entry for Apps/Technical (Closes: #128764)

 -- Margarita Manterola <marga@debian.org>  Wed,  6 Dec 2006 11:11:48 -0300

xcircuit (3.6.24-1) unstable; urgency=low

  * QA Upload
  * New Upstream Version (Closes: #266080, #262321)
  * Update debian/copyright
  * debian/{menu, install}:
    + install pixmaps to /usr/share/pixmaps/xcircuit/
  * Conforms with new Standards version 3.7.2

 -- Michael Ablassmeier <abi@debian.org>  Mon, 22 May 2006 10:08:25 +0200

xcircuit (3.1.19-2) unstable; urgency=low

  * QA Group upload orphaning this package
  * debian/changelog: removed obsolete crap from the bottom
  * debian/menu: quoted the unquoted
  * Updated config.{sub,guess}

 -- Andrew Pollock <apollock@debian.org>  Sun,  9 Apr 2006 14:52:08 -0700

xcircuit (3.1.19-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove Build-Dependency on xlibs-dev (Closes: #346798).
  * Credit and Big Thanks to Justin Pryzby <justinpryzby@users.sourceforge.net>
    for the patch and testing.

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Sat, 21 Jan 2006 18:07:15 +0100

xcircuit (3.1.19-1) unstable; urgency=low

  * New upstream release.

 -- David Z Maze <dmaze@debian.org>  Sun, 24 Aug 2003 09:08:10 -0400

xcircuit (3.1.18-1) unstable; urgency=low

  * New upstream release.
  * Standards-Version 3.6.0 (no changes).
  * Build vs. Python 2.3.

 -- David Z Maze <dmaze@debian.org>  Sat,  9 Aug 2003 10:59:26 -0400

xcircuit (3.1.15-1) unstable; urgency=low

  * New upstream release.
  * Migrate from CBS to cdbs.  Thanks to Colin Walters for creating the
    infrastructure, and for giving tips on setting up debian/rules.

 -- David Z Maze <dmaze@debian.org>  Thu, 29 May 2003 14:44:08 -0400

xcircuit (3.1.14-1) unstable; urgency=low

  * New upstream release.
  * Update to CBS 1.59.
  * Standards-Version 3.5.10.0 (no changes).

 -- David Z Maze <dmaze@debian.org>  Sun, 18 May 2003 21:00:19 -0400

xcircuit (3.1.12-1) unstable; urgency=low

  * New upstream release.
  * Update to CBS 1.57.
  * Should build from source (not tripping over internal gcc errors)
    on hppa.  (Closes: #189583)

 -- David Z Maze <dmaze@debian.org>  Mon, 21 Apr 2003 15:26:08 -0400

xcircuit (3.1.10-1) unstable; urgency=low

  * New upstream release.
  * Update to CBS 1.56.
  * Standards-Version 3.5.9.0 (no changes).

 -- David Z Maze <dmaze@debian.org>  Sun, 13 Apr 2003 13:57:35 -0400

xcircuit (3.0rev8-1) unstable; urgency=low

  * New upstream release.
  * Update to CBS 1.50.

 -- David Z Maze <dmaze@debian.org>  Sun, 19 Jan 2003 10:57:32 -0500

xcircuit (3.0rev7-1) unstable; urgency=low

  * New upstream release.
  * Update to CBS 1.48.

 -- David Z Maze <dmaze@debian.org>  Wed, 18 Dec 2002 23:58:36 -0500

xcircuit (3.0rev5-2) unstable; urgency=low

  * Use Colin's Build System.  This includes finally switching the package
    to using debhelper.

 -- David Z Maze <dmaze@debian.org>  Sat,  7 Dec 2002 11:47:30 -0500

xcircuit (3.0rev5-1) unstable; urgency=low

  * New upstream release.
  * Updated to standards-version 3.5.8.0 (no changes).

 -- David Z Maze <dmaze@debian.org>  Fri,  6 Dec 2002 23:35:31 -0500

xcircuit (3.0rev4-1) unstable; urgency=low

  * New upstream release.  This includes a fix to a critical bug (though
    with a goto rather than a continue).  (Closes: #166509)
  * Switch to using the default version of Python, rather than forcing
    python2.1.
  * No longer manage a /usr/doc link.  (These things are much easier when
    you use debhelper.  :-)

 -- David Z Maze <dmaze@debian.org>  Sat, 23 Nov 2002 10:24:39 -0500

xcircuit (2.5.5rev0-1) unstable; urgency=low

  * New upstream release.

 -- David Z Maze <dmaze@debian.org>  Wed, 29 May 2002 23:30:23 -0400

xcircuit (2.5.3rev0-1) unstable; urgency=low

  * New upstream release.

 -- David Z Maze <dmaze@debian.org>  Sat,  5 Jan 2002 19:37:48 -0500

xcircuit (2.5.2rev1-1) unstable; urgency=low

  * New upstream release.
  * Add a build dependency on m4; it's needed to generate the man page.

 -- David Z Maze <dmaze@debian.org>  Sun, 30 Dec 2001 18:39:56 -0500

xcircuit (2.5.2rev0-1) unstable; urgency=low

  * New upstream release.

 -- David Z Maze <dmaze@debian.org>  Mon, 24 Dec 2001 11:49:33 -0500

xcircuit (2.5.1rev1-1) unstable; urgency=low

  * New upstream release.  License changed from Artistic to GPL.

 -- David Z Maze <dmaze@debian.org>  Sat, 15 Dec 2001 10:11:38 -0500

xcircuit (2.4.0rev2-1) unstable; urgency=low

  * New upstream release.  Includes correct location for tutorial in
    xcircuit.1 (Closes: #114752).
  * Added an icon to the menu item.
  * Updated debian/rules to deal correctly with auto*.
  * Use Python 2.1.

 -- David Z Maze <dmaze@debian.org>  Thu, 22 Nov 2001 09:06:56 -0500

xcircuit (2.3.3-1) unstable; urgency=low

  * New upstream release.
  * Updated to Standards-Version 3.5.5.0 (no changes).

 -- David Z Maze <dmaze@debian.org>  Wed, 11 Jul 2001 17:18:54 -0400

xcircuit (2.3-1) unstable; urgency=low

  * New upstream release.
  * Let ld bring in Python's dependencies automatically; don't explicitly
    build-depend on e.g. libdb2-dev which isn't directly used by xcircuit.

 -- David Z Maze <dmaze@debian.org>  Sat, 26 May 2001 08:48:03 -0400

xcircuit (2.2.2-3) unstable; urgency=low

  * Move the X resource file to /etc/X11/app-defaults.  Hopefully this
    addresses bug #94996.
  * Updated to standards-version 3.5.3.0.

 -- David Z Maze <dmaze@debian.org>  Mon, 23 Apr 2001 21:58:17 -0400

xcircuit (2.2.2-2) unstable; urgency=low

  * Added a build dependency on libdb2-dev.  (Closes: #90259)  Looking
    at the things xcircuit links against, this should be the only missing
    build dependency.

  * I don't think xcircuit falls into any of the categories for "extra"
    packages in policy (sec. 2.2), so upgrade priority to "optional".

 -- David Z Maze <dmaze@debian.org>  Mon, 19 Mar 2001 21:40:35 -0500

xcircuit (2.2.2-1) unstable; urgency=low

  * New upstream version.  User-defined keybindings (introduced in
    2.2.1) can now include mouse buttons, and the help screen reflects
    user setup.  Improved Python interface.  Fixed a PostScript bug.

 -- David Z Maze <dmaze@debian.org>  Tue, 13 Mar 2001 23:51:47 -0500

xcircuit (2.2.1-2) unstable; urgency=low

  * Build-depend on python2-dev.  (Closes: #85759)

 -- David Z Maze <dmaze@mit.edu>  Mon, 12 Feb 2001 22:51:17 -0500

xcircuit (2.2.1-1) unstable; urgency=low

  * New upstream version.  Major change: some internal use of Python as
    a start on replacing the configuration language.
  * Updated to Standards-Version 3.5.0.0.
  * Move the library files to /usr/share/xcircuit, since they're
    platform-independent and we don't worry about multiple versions.
  * Move the X resources file to /etc/X11/Xresources, as per policy.
  * Strip the xcircuit binary enough to appease lintian.

 -- David Z Maze <dmaze@mit.edu>  Tue,  6 Feb 2001 22:45:07 -0500

xcircuit (2.2.0-1) unstable; urgency=low

  * New maintainer.  (Closes: #80358)
  * New upstream release.  (Closes: #60608, #74043)
  * Updated to Standards-Version 3.2.1.0.  Moved stuff into /usr/share
    as appropriate, and added correct language in postinst/prerm to
    deal with /usr/doc link.  Moved update-menus call to postrm.
  * Move the binary from /usr/X11R6/bin to /usr/bin.
  * Install app-defaults file correctly in /etc/X11/app-defaults.
    (Closes: #80854)
  * Copy all of the upstream README files to /usr/share/doc/xcircuit.
  * Now Lintian-clean.  Yay!  (Closes: #73019)
  * Permissions on everything look correct now.  (Closes: #70822)
  * Added "Vector" hint to menu entry.  (Closes: #80056)
  * Remove menu entry for xschema.  (Closes: #79054)  Also remove mention
    of xschema from description.

 -- David Z Maze <dmaze@mit.edu>  Tue, 16 Jan 2001 20:27:46 -0500

xcircuit (2.0b1-2) frozen unstable; urgency=low

  * Applied Ben Collins' patch for app defaults. closes: #59571

 -- Dale Scheetz <dwarf@polaris.net>  Sat,  4 Mar 2000 19:19:49 -0500

xcircuit (2.0b1-1) unstable; urgency=low

  * new upstream source
  * Patched install process to put bins in X11R6/bin instead of
  *        bin. closes: #49915
  * Commented out default apps install in Imakefile, letting the
  *        rules file install it properly. closes: #50573

 -- Dale Scheetz <dwarf@polaris.net>  Sat,  1 Jan 2000 16:21:46 -0500

xcircuit (2.0a11-1) unstable; urgency=low

  * new upstream source:
  *                 closes: #34966, #37019, #41834

 -- Dale Scheetz <dwarf@polaris.net>  Tue,  9 Nov 1999 18:33:45 -0500

xcircuit (2.0a6-3) frozen unstable; urgency=high

  * retarget package for frozen to propogate previous fix into stable

 -- Dale Scheetz <dwarf@polaris.net>  Wed, 13 Jan 1999 19:35:29 -0500

xcircuit (2.0a6-2) unstable; urgency=low

  * recompiled with glibc 2.0.7u-7 to follow __register_frame_info fix

 -- Dale Scheetz <dwarf@polaris.net>  Wed,  2 Dec 1998 16:21:43 -0500

xcircuit (2.0a6-1) unstable; urgency=low

  * new upstream source
  *    includes fix for "segfault when breaking a line": fixes 21727
  * added menu entry for Xschema for access to the new schematic code.

 -- Dale Scheetz <dwarf@polaris.net>  Mon, 16 Nov 1998 10:36:44 -0500

xcircuit (1.7-5) unstable; urgency=low

  * recompiled against correct libc6 shlibs file for proper dependencies.

 -- Dale Scheetz <dwarf@polaris.net>  Sat,  3 Oct 1998 16:05:28 -0400

xcircuit (1.7-4) unstable; urgency=low

  * applied Paul Slootman's patch repairing the return value of
  *      _XwMapFromHex to unsigned long, so it can cast to a pointer
  *      on the alpha. Also replaced references of <strings.h> with the
  *      more correct reference to <string.h>: fixes 22650
  * Removed XCircuit.ad from the Imakefile, leaving the installation
  *      of this file to the rules file: fixes 24856

 -- Dale Scheetz <dwarf@polaris.net>  Sun, 27 Sep 1998 14:45:46 -0400

xcircuit (1.7-3) frozen unstable; urgency=low

  * rules clean target now removes substvars* and files*: fixes 21216

 -- Dale Scheetz <dwarf@polaris.net>  Sat, 18 Apr 1998 22:03:50 -0400

xcircuit (1.7-2) frozen unstable; urgency=low

  * Added XCircuit.ad for proper color defaults: fixes 20142 and 20933
  * Fixed clean target in rules for clean build: fixes 20970

 -- Dale Scheetz <dwarf@polaris.net>  Sat, 11 Apr 1998 12:29:46 -0400

xcircuit (1.7-1) unstable; urgency=low

  * New package from upstream sources

 -- Dale Scheetz <dwarf@polaris.net>  Sat,  7 Mar 1998 20:55:26 -0500
